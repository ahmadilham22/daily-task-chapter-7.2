import React from 'react'
import AppBar from '@mui/material/AppBar';
import Toolbar from "@mui/material/Toolbar";
import AndroidIcon from '@mui/icons-material/Android';
import Tab from '@mui/material/Tab';
import TabList from "@mui/lab/TabList"
// import TabList from '@mui/lab/TabList';


const Navbar = () => {
  return (
    <React.Fragment>
        <AppBar sx={{ background: "#063970" }}>
            <Toolbar>
                    <AndroidIcon/>
                    {/* <TabList  aria-label="lab API tabs example">
                        <Tab label="Item One" value="1" />
                        <Tab label="Item Two" value="2" />
                        <Tab label="Item Three" value="3" />
                    </TabList> */}
            </Toolbar>            
        </AppBar>
    </React.Fragment>
  )
}

export default Navbar